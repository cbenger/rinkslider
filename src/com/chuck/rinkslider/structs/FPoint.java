package com.chuck.rinkslider.structs;

/**
 * Created with IntelliJ IDEA.
 * User: Charles
 * Date: 1/28/13
 * Time: 5:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class FPoint {

    private float x;
    private float y;


    public FPoint(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getY() {
        return y;
    }

    public void set(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }
}
