package com.chuck.rinkslider.entity;

import com.chuck.rinkslider.structs.FPoint;
import com.chuck.rinkslider.util.MyLogger;
import org.andengine.entity.primitive.Rectangle;

/**
 * Purpose:
 * User: cbenger
 * Date: 1/22/13
 * Time: 5:49 PM
 */
public class Navigator {

    private static Navigator ourInstance;
    private static final byte NUMBER_OF_NAVIGATION_BARS = 3; //A tile can at most move 3 directions
    private Grid gridInstance;
    private Rectangle[] navigationBars = new Rectangle[NUMBER_OF_NAVIGATION_BARS];

    public static Navigator getInstance() {
        if(ourInstance == null)
            ourInstance = new Navigator();

        return ourInstance;
    }

    private Navigator() {
        gridInstance = Grid.getOutInstance();
        for(int i = 0; i < NUMBER_OF_NAVIGATION_BARS; i++)
            navigationBars[i] = new Rectangle(0,0,0,0, gridInstance.getVertexBufferObjectManager());
    }

    public void attachNavigationBarsToGrid() {
       for(Rectangle navigationBar : navigationBars)
           gridInstance.attachChild(navigationBar);
    }

    public void showNavigationBarsFor(UserTile playerTile) {
        MyLogger.log(playerTile.toString());
        int navCounter = 0;
        FPoint[] points = gridInstance.getCollisionPathsAround(playerTile);
        for(FPoint point : points) {
            if(point != null) {
                navigationBars[navCounter].setWidth(point.getX() - playerTile.getX() + playerTile.getWidth());
                navigationBars[navCounter].setHeight(playerTile.getHeight());
                navigationBars[navCounter].setX(playerTile.getX() + playerTile.getWidth());
                navigationBars[navCounter].setY(playerTile.getY());
            }
        }
    }

    //private void checkTiles(Tile )

}
