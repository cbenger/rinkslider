package com.chuck.rinkslider.entity;

import com.chuck.rinkslider.config.Resource;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.BaseGameActivity;

/**
 * Purpose:
 * User: chuck
 * Date: 21/01/13
 * Time: 9:38 PM
 */
public class GlowLine extends Sprite {

    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private static TextureRegion textureRegion;

    // ===========================================================
    // Constructors
    // ===========================================================

    public GlowLine(float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pWidth, pHeight, textureRegion, pVertexBufferObjectManager);
    }


    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    public static void loadTexture(BitmapTextureAtlas bitmapTextureAtlas, BaseGameActivity context) {
        textureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bitmapTextureAtlas, context, Resource.GLOW_LINE_BASE, 0, 0);
        textureRegion.setTexturePosition(25, 19);
        textureRegion.setTextureSize(20, 568);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
