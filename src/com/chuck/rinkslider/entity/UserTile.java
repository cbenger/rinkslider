package com.chuck.rinkslider.entity;

import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

/**
 * Created with IntelliJ IDEA.
 * User: Charles
 * Date: 1/28/13
 * Time: 4:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserTile extends Tile {

    private int rowIndex;
    private int columnIndex;

    public UserTile(int rowIndex, int columnIndex, float pX, float pY, float size, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, size, pVertexBufferObjectManager);
        this.setColor(Color.YELLOW);
        this.rowIndex = rowIndex;
        this.columnIndex = columnIndex;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        if (pSceneTouchEvent.isActionDown()) {
            Navigator navigator = Navigator.getInstance();
            navigator.showNavigationBarsFor(this);
        }
        return true;
    }
}
