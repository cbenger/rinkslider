package com.chuck.rinkslider.entity;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

/**
 * Purpose:
 * User: cbenger
 * Date: 1/22/13
 * Time: 4:43 PM
 */
public class Tile extends Rectangle {

    public Tile(float pX, float pY, float size, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, size, size, pVertexBufferObjectManager);
        setColor(Color.TRANSPARENT);
    }

    @Override
    public String toString() {
        return String.format("x = %s y = %s width = %s height = %s", getX(), getY(),getWidth(), getHeight());
    }
}

