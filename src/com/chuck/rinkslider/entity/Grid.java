package com.chuck.rinkslider.entity;

import android.graphics.Point;
import com.chuck.rinkslider.MainActivity;
import com.chuck.rinkslider.config.GameOptions;
import com.chuck.rinkslider.structs.FPoint;
import com.chuck.rinkslider.util.MyLogger;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;

import java.util.ArrayList;

/**
 * Purpose: Grid that encapsulates all game tiles
 * User: chuck
 * Date: 21/01/13
 * Time: 8:15 PM
 */
public final class Grid extends Rectangle {

    private final static byte DEFAULT_GRID_SIZE = 7;
    private final static byte GRID_PADDING_PIXELS = 20;
    private final static byte REQUIRED_EDGE_TILES = 3;
    private float tileSize;
    private byte tilesPerSide;
    private byte numberOfTiles;
    private MainActivity activityReference;
    private static Grid ourInstance = null;
    private Tile[][] gameTiles;

    public Grid(byte numberOfTiles, float pX, float pY, float size, MainActivity activityReference) {
        super(pX, pY, size, size, activityReference.getVertexBufferObjectManager());
        this.activityReference = activityReference;
        calculateGridSetup(numberOfTiles);
        createAndAttachTiles();
    }

    public static Grid getOutInstance() {
        return ourInstance;
    }

    public Tile[][] getGameTiles() {
        return gameTiles;
    }

    public static Grid createUsingBestPosition(MainActivity activity) {
        if (ourInstance == null) {
            OptimalPosition position = new OptimalPosition(GameOptions.cameraWidth, GameOptions.cameraHeight);
            position.calculate(GRID_PADDING_PIXELS);
            ourInstance = new Grid(DEFAULT_GRID_SIZE, position.getOffsetX(), position.getOffsetY(), position.getOptimalSize(), activity);
        }
        return ourInstance;
    }

    public void drawGridLines() {
        drawRows();
        drawColumns();
    }

    private void drawRows() {
        for (float offsetY = 0; offsetY <= getHeight(); offsetY += tileSize)
            attachChild(new Line(0, offsetY, getWidth(), offsetY, activityReference.getVertexBufferObjectManager()));
    }

    private void drawColumns() {
        for (float offsetX = 0; offsetX <= getWidth(); offsetX += tileSize)
            attachChild(new Line(offsetX, 0, offsetX, getHeight(), activityReference.getVertexBufferObjectManager()));
    }

    private void calculateGridSetup(byte numberOfTiles) {
        this.tileSize = getHeight() / numberOfTiles;
        this.numberOfTiles = numberOfTiles;
        this.tilesPerSide = (byte) ((numberOfTiles - REQUIRED_EDGE_TILES)/2);
    }

    private void createAndAttachTiles() {
        gameTiles = new Tile[numberOfTiles][numberOfTiles];

        for (int row = 0; row < gameTiles.length; row++) {
            for (int col = 0; col < gameTiles[row].length; col++) {

                if (isAlongTopOrBottomEdge(row, col) || isAlongLeftOrRightEdge(row, col) ) {

                    float xOffset = col * tileSize;
                    float yOffset = row * tileSize;

                    UserTile tile = new UserTile(row, col,xOffset , yOffset, tileSize, activityReference.getVertexBufferObjectManager());
                    gameTiles[row][col] = tile;
                    attachChild(tile);

                } else {
                    //Non visual tile
                    gameTiles[row][col] = new Tile(0,0,0,activityReference.getVertexBufferObjectManager());
                }
            }
        }
    }

    private boolean isAlongTopOrBottomEdge(int row, int col) {
        return (row == 0 || row == gameTiles.length - 1) && (col < tilesPerSide   || col >= gameTiles[row].length - tilesPerSide);
    }

    private boolean isAlongLeftOrRightEdge(int row, int col) {
        return ((col == 0 || col == gameTiles[row].length - 1) && (row < tilesPerSide || row >= gameTiles[row].length - tilesPerSide));
    }

    public void registerTileTouchEvent(Scene pScene) {
        for (int row = 0; row < gameTiles.length; row++)
            for (int col = 0; col < gameTiles[row].length; col++)
                if(gameTiles[row][col] instanceof UserTile)
                     pScene.registerTouchArea(gameTiles[row][col]);
    }

    public FPoint[] getCollisionPathsAround(UserTile userTile) {
        final int MAX_POSSIBLE_COLLISION_POINTS = 3;
        FPoint[] collisionPoints = new FPoint[MAX_POSSIBLE_COLLISION_POINTS];
        int offset = 0;

        FPoint rightCollision = getCollisionsToRightOf(userTile);

        if(rightCollision != null) {
            collisionPoints[offset] = rightCollision;
            offset++;
        }

        return collisionPoints;
    }

    private FPoint getCollisionsToRightOf(UserTile userTile) {
        FPoint point = null;

        int tileRow = userTile.getRowIndex();
        int tileCol = userTile.getColumnIndex();
        for(int col = tileCol + 1; col < gameTiles[tileRow].length; col++) {
            Tile tile = gameTiles[tileRow][tileCol];
             if(tile instanceof UserTile) {
                if(col != tileCol + 1 || col == gameTiles[tileRow].length - 1)
                {
                    point = new FPoint(col * tileSize, tileRow * tileSize);
                    break;
                }
             }
        }

        return point;
    }


    /**
     * Used to calculate the optimal size and center position for the grid
     */
    private final static class OptimalPosition {

        private float offsetX;
        private float offsetY;
        private int optimalSize;
        private int width;
        private int height;

        private OptimalPosition(int width, int height) {
            this.height = height;
            this.width = width;
        }

        public void calculate(byte padding) {
            calculateBestSize(padding);
            calculateCenter();
            MyLogger.log(this.toString());
        }

        private void calculateBestSize(byte padding) {
            optimalSize = (width < height ? width : height) - padding;
        }

        private void calculateCenter() {

            final float centerX = width / 2.0f;
            final float centerY = height / 2.0f;
            final float gridCenter = optimalSize / 2.0f;

            offsetX = centerX - gridCenter;
            offsetY = centerY - gridCenter;
        }

        public float getOffsetX() {
            return offsetX;
        }

        public float getOffsetY() {
            return offsetY;
        }

        public int getOptimalSize() {
            return optimalSize;
        }

        @Override
        public String toString() {
            return "OptimalPosition{" +
                    "offsetX=" + offsetX +
                    ", offsetY=" + offsetY +
                    ", optimalSize=" + optimalSize +
                    ", cam width=" + width +
                    ", cam height=" + height +
                    '}';
        }
    }
}



















