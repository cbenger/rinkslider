package com.chuck.rinkslider;

import android.view.Display;
import com.chuck.rinkslider.config.GameOptions;
import com.chuck.rinkslider.entity.GlowLine;
import com.chuck.rinkslider.entity.Grid;
import com.chuck.rinkslider.entity.Navigator;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.color.Color;


public class MainActivity extends BaseGameActivity {


    private Camera gameCamera;
    private BitmapTextureAtlas bitmapTextureAtlas;


    @Override
    public EngineOptions onCreateEngineOptions() {
        setupGameOptionState();
        gameCamera = new Camera(0, 0, GameOptions.cameraWidth, GameOptions.cameraHeight);

        return new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(), gameCamera);
    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws Exception {

        bitmapTextureAtlas = new BitmapTextureAtlas(getTextureManager(), 128, 600, TextureOptions.DEFAULT);
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        GlowLine.loadTexture(bitmapTextureAtlas, this);

        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }

    @Override
    public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws Exception {
        final Scene scene = new Scene();
        pOnCreateSceneCallback.onCreateSceneFinished(scene);
    }

    @Override
    public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
        pScene.setBackground(new Background(Color.BLACK));

        Grid grid = Grid.createUsingBestPosition(this);
        grid.drawGridLines();
        grid.setColor(Color.RED);
        grid.registerTileTouchEvent(pScene);

        Navigator navigator = Navigator.getInstance();
        navigator.attachNavigationBarsToGrid();

        pScene.setTouchAreaBindingOnActionDownEnabled(true);
        pScene.attachChild(grid);

        pOnPopulateSceneCallback.onPopulateSceneFinished();
    }


    private void setupGameOptionState() {
        final Display display = getWindowManager().getDefaultDisplay();
        GameOptions.cameraWidth = display.getWidth();
        GameOptions.cameraHeight = display.getHeight();
    }


}
