package com.chuck.rinkslider.util;

import android.util.Log;
import com.chuck.rinkslider.config.GameOptions;

/**
 * Purpose: Provides a static method to write to the debug console
 * User: chuck
 * Date: 21/01/13
 * Time: 8:45 PM
 */
public class MyLogger {


    /**
     * Simple Logging interface
     * @param message the message to write to the debug console
     */
    public static void log(String message) {
        if (GameOptions.DEBUG) Log.e("MyLogger", message);
    }

}
