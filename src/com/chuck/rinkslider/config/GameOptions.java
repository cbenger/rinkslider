package com.chuck.rinkslider.config;

/**
 * User: cbenger
 * Date: 1/21/13
 * Time: 2:04 PM
 */
public class GameOptions {

    public static int cameraWidth = 720;
    public static int cameraHeight = 420;
    public final static boolean DEBUG = true;
}
